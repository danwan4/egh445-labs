clear;
close all;

%% Define model parameters
m = 0.15; % Pendulum mass (kg)
Mc = 0.4; % Cart mass (kg)
l = 0.2; % Pendulum arm length (m)
g = 9.81; % Gravity (m/s^2)

% Equilibrium point A
x1_bar = 0;
x2_bar = 0;
x3_bar = 0;
x4_bar = 0;

% Initial conditions
x0 = [0.2, 20*pi/180, 0, 0]';

%% Compute matrices for linearised model about equilibrium point A
Aa = [0, 0, 1, 0; 
      0, 0, 0, 1; 
      0, -m*g/Mc, 0, 0; 
      0, g*(Mc+m)/(l*Mc), 0, 0];
Ba = [0;
      0;
      1/Mc;
      -1/(l*Mc)];
Ca = [1, 0, 0, 0; 
      0, 1, 0, 0];
Da = zeros(2, 1);

%% Verify controllability
controllable = rank(ctrb(Aa, Ba)) == 4;

%% Compute controller gain for desired eigenvalues -3, -4, -5, -6
Pa = [-3, -4, -5, -6];
Ka = place(Aa, Ba, Pa);

%% Verify observability
observable = rank(obsv(Aa, Ca)) == 4;

%% Compute observer gain for desired eigenvalues -63, -64, -65, -66
Pa = [-63, -64, -65, -66];
La = place(Aa', Ca', Pa)';

%% Simulate linearised model
% Offset initial conditions for equilibrium point
x0lin = [x0(1) - x1_bar, x0(2) - x2_bar, x0(3) - x3_bar, x0(4) - x4_bar]';

sim('CP_ContrSys_Lin_a_9215719');

% Offset results for equilibrium point
x1a = x1a + x1_bar;
x2a = x2a + x2_bar;
x3a = x3a + x3_bar;
x4a = x4a + x4_bar;
x1ahat = x1ahat + x1_bar;
x2ahat = x2ahat + x2_bar;
x3ahat = x3ahat + x3_bar;
x4ahat = x4ahat + x4_bar;

%% Simulate nonlinear model
K_SF = Ka;
L_NL = La;
A_NL = Aa;
B_NL = Ba;
C_NL = Ca;
sim('CP_ContrSys_NLin_9215719');

%% Plot
figure;

subplot(5, 1, 1);
plot(t,x1,'b',t,x1hat,'g',ta,x1a,'r--',ta,x1ahat,'k--');
title('Equilibrium Point A');
% title('x_1');
xlabel('Time (s)');
ylabel('Cart Position (m)');
legend('x_1 Nonlinear', 'x_1 hat Nonlinear', 'x_1 Linearised', 'x_1 hat Linearised');
grid on;

subplot(5, 1, 2);
plot(t,x2*180/pi,'b',t,x2hat*180/pi,'g',ta,x2a*180/pi,'r--',ta,x2ahat*180/pi,'k--');
% title('x_2');
xlabel('Time (s)');
ylabel('Pendulum Angle (deg)');
legend('x_2 Nonlinear', 'x_2 hat Nonlinear', 'x_2 Linearised', 'x_2 hat Linearised');
grid on;

subplot(5, 1, 3);
plot(t,x3,'b',t,x3hat,'g',ta,x3a,'r--',ta,x3ahat,'k--');
% title('x_3');
xlabel('Time (s)');
ylabel('Cart Velocity (m/s)');
legend('x_3 Nonlinear', 'x_3 hat Nonlinear', 'x_3 Linearised', 'x_3 hat Linearised');
grid on;

subplot(5, 1, 4);
plot(t,x4*180/pi,'b',t,x4hat*180/pi,'g',ta,x4a*180/pi,'r--',ta,x4ahat*180/pi,'k--');
% title('x_4');
xlabel('Time (s)');
ylabel('Pendulum Rate (deg/s)');
legend('x_4 Nonlinear', 'x_4 hat Nonlinear', 'x_4 Linearised', 'x_4 hat Linearised');
grid on;

subplot(5, 1, 5);
plot(t,F,'b',ta,Fa,'r--');
% title('Force');
xlabel('Time (s)');
ylabel('Control Force (N)');
legend('F Nonlinear', 'F Linearised');
grid on;

%% Animation
% Cart_Pendulum_Animation(t, x1, x2, 0, 0);
% Cart_Pendulum_Animation(ta, x1a, x2a, 0, 0);