clear;
close all;

%% Define model parameters
m = 0.15; % Pendulum mass (kg)
Mc = 0.4; % Cart mass (kg)
l = 0.2; % Pendulum arm length (m)
g = 9.81; % Gravity (m/s^2)

% Equilibrium point B
x1_bar = 0;
x2_bar = pi;
x3_bar = 0;
x4_bar = 0;

% Initial conditions
x0 = [0.2, 200*pi/180, 0, 0]';

%% Compute matrices for linearised model about equilibrium point B
Ab = [0, 0, 1, 0; 
      0, 0, 0, 1; 
      0, -m*g/Mc, 0, 0; 
      0, -g*(Mc+m)/(l*Mc), 0, 0];
Bb = [0; 
      0;
      1/Mc;
      1/(l*Mc)];
Cb = [1, 0, 0, 0;
      0, 1, 0, 0];
Db = zeros(2, 1);

%% Verify controllability
controllable = rank(ctrb(Ab, Bb)) == 4;

%% Compute controller gain for desired eigenvalues -3, -4, -5, -6
Pb = [-3, -4, -5, -6];
Kb = place(Ab, Bb, Pb);

%% Verify observability
observable = rank(obsv(Ab, Cb)) == 4;

%% Compute observer gain for desired eigenvalues -63, -64, -65, -66
Pb = [-63, -64, -65, -66];
Lb = place(Ab', Cb', Pb)';

%% Simulate linearised model
% Offset initial conditions for equilibrium point
x0lin = [x0(1) - x1_bar, x0(2) - x2_bar, x0(3) - x3_bar, x0(4) - x4_bar]';

sim('CP_ContrSys_Lin_b_9215719');

% Offset results for equilibrium point
x1b = x1b + x1_bar;
x2b = x2b + x2_bar;
x3b = x3b + x3_bar;
x4b = x4b + x4_bar;
x1bhat = x1bhat + x1_bar;
x2bhat = x2bhat + x2_bar;
x3bhat = x3bhat + x3_bar;
x4bhat = x4bhat + x4_bar;

%% Simulate nonlinear model
K_SF = Kb;
L_NL = Lb;
A_NL = Ab;
B_NL = Bb;
C_NL = Cb;
sim('CP_ContrSys_NLin_9215719');

%% Plot
figure;

subplot(5, 1, 1);
plot(t,x1,'b',t,x1hat,'g',tb,x1b,'r--',tb,x1bhat,'k--');
title('Equilibrium Point B');
% title('x_1');
xlabel('Time (s)');
ylabel('Cart Position (m)');
legend('x_1 Nonlinear', 'x_1 hat Nonlinear', 'x_1 Linearised', 'x_1 hat Linearised');
grid on;

subplot(5, 1, 2);
plot(t,x2*180/pi,'b',t,x2hat*180/pi,'g',tb,x2b*180/pi,'r--',tb,x2bhat*180/pi,'k--');
% title('x_2');
xlabel('Time (s)');
ylabel('Pendulum Angle (deg)');
legend('x_2 Nonlinear', 'x_2 hat Nonlinear', 'x_2 Linearised', 'x_2 hat Linearised');
grid on;

subplot(5, 1, 3);
plot(t,x3,'b',t,x3hat,'g',tb,x3b,'r--',tb,x3bhat,'k--');
% title('x_3');
xlabel('Time (s)');
ylabel('Cart Velocity (m/s)');
legend('x_3 Nonlinear', 'x_3 hat Nonlinear', 'x_3 Linearised', 'x_3 hat Linearised');
grid on;

subplot(5, 1, 4);
plot(t,x4*180/pi,'b',t,x4hat*180/pi,'g',tb,x4b*180/pi,'r--',tb,x4bhat*180/pi,'k--');
% title('x_4');
xlabel('Time (s)');
ylabel('Pendulum Rate (deg/s)');
legend('x_4 Nonlinear', 'x_4 hat Nonlinear', 'x_4 Linearised', 'x_4 hat Linearised');
grid on;

subplot(5, 1, 5);
plot(t,F,'b',tb,Fb,'r--');
% title('Force');
xlabel('Time (s)');
ylabel('Control Force (N)');
legend('F Nonlinear', 'F Linearised');
grid on;

%% Animation
% Cart_Pendulum_Animation(tb, x1b, x2b, 0, pi);
% Cart_Pendulum_Animation(t, x1, x2, 0, pi);