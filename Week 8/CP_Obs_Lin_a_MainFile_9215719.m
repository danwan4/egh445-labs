clear;
clc;

%% Define parameters
m = 0.15;
Mc = 0.4;
l = 0.2;
g = 9.81;

%% Compute matrices
Aa = [0, 0, 1, 0; 
    0, 0, 0, 1; 
    0, -m*g/Mc, 0, 0; 
    0, g*(Mc+m)/(l*Mc), 0, 0];
Ba = [0; 0; 1/Mc; -1/(l*Mc)];
Ca = [1, 0, 0, 0; 0, 1, 0, 0];
Da = zeros(2, 1);

%% Gain from last lab
Pa = [-3, -4, -5, -6];
Ka = place(Aa, Ba, Pa);

%% Determine observability
observable = rank(obsv(Aa, Ca)) == 4;

%% Computer observer gain for eigenvalues -63, -64, -65, -66
Pa = [-63, -64, -65, -66];
La = place(Aa', Ca', Pa)';

%% Simulate linear model
x0 = [0.2; 20*pi/180; 0; 0];
sim('CP_Obs_Lin_a_9215719');

%% 
K_SF = Ka;
L_NL = La;
A_NL = Aa;
B_NL = Ba;
C_NL = Ca;

x1_bar = 0;
x2_bar = 0;
x3_bar = 0;
x4_bar = 0;


figure;

subplot(4, 1, 1);
title('x1');
plot(ta,x1a,'r',ta,x1ahat,'b');

subplot(4, 1, 2);
title('x2');
plot(ta,x2a,'r',ta,x2ahat,'b');

subplot(4, 1, 3);
title('x3');
plot(ta,x3a,'r',ta,x3ahat,'b');

subplot(4, 1, 4);
title('x4');
plot(ta,x4a,'r',ta,x4ahat,'b');

%% Simulate nonlinear model
sim('CP_Obs_NLin_9215719');

%% Plot
figure;

subplot(4, 1, 1);
title('x1');
plot(t,x1,'r',t,x1hat,'b');

subplot(4, 1, 2);
title('x2');
plot(t,x2,'r',t,x2hat,'b');

subplot(4, 1, 3);
title('x3');
plot(t,x3,'r',t,x3hat,'b');

subplot(4, 1, 4);
title('x4');
plot(t,x4,'r',t,x4hat,'b');