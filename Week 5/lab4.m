clear

% Two sets of values indicated by '1' for Set 1 and '2' for Set 2

m_1 = 1;
c_1 = 1;
b_1 = 1;

m_2 = 1;
c_2 = 1;
b_2 = 100;

A_1 = [0, 1; -1/(c_1*m_1), -b_1/m_1];
A_2 = [0, 1; -1/(c_2*m_2), -b_2/m_2];

B_1 = [0; 1/m_1];
B_2 = [0; 1/m_2];

C = [1, 0; 0, 1];

D = [0; 0];

eig_1 = eig(A_1)
eig_2 = eig(A_2)

% All eigenvalues have negative real parts, indicating stiffness.
% However, A1's values aren't spread so stiffness ratio is low.
% Real part of A2's eigenvalues are very spread indicating high stiffness
% ratio and that it has both very fast and very slow dynamics.

% Time stamp: Inverse of absolute value of greatest real part of eigenvalue
% / some arbitrary constant for level of accuracy e.g. 10
timestamp_1 = abs(-0.5)^-1 / 10
timestamp_2 = abs(-99.99)^-1 / 10

% Simulation time: 5 * (inverse of absolute value of smallest real part of
% an eigenvalue)
simtime_1 = 5 * abs(-0.5)^-1
simtime_2 = 5 * abs(-0.01)^-1