clear;
close all;

%% Define model parameters
m = 0.15; % Pendulum mass (kg)
Mc = 0.4; % Cart mass (kg)
l = 0.2; % Pendulum arm length (m)
g = 9.81; % Gravity (m/s^2)
x0 = [0.2; 20*pi/180; 0; 0]; % Initial conditions

%% Compute matrices for linearised model about equilibrium point A
Aa = [0, 0, 1, 0; 
      0, 0, 0, 1; 
      0, -m*g/Mc, 0, 0; 
      0, g*(Mc+m)/(l*Mc), 0, 0];
Ba = [0;
      0;
      1/Mc;
      -1/(l*Mc)];
Ca = eye(4);
Da = zeros(4, 1);

%% Create continuous-time state-space model
sysc = ss(Aa, Ba, Ca, Da);

%% Create discrete-time model
Ts = 0.03;
sysdzoh1 = c2d(sysc, Ts, 'zoh');
Ada = sysdzoh1.A;
Bda = sysdzoh1.B;
Cda = sysdzoh1.C;
Dda = sysdzoh1.D;

%% Caclulate poles -3, -4, -5, -6 in Z-domain
Pda = [exp(-3*Ts), exp(-4*Ts), exp(-5*Ts), exp(-6*Ts)];
Kda = place(Ada, Bda, Pda);

%% Simulate discrete time model
sim('CP_DTSFC_Lin_a_9215719');

%% Simulate sampled-data continuous-time model
Ka = Kda;
sim('CP_SD_SFC_Lin_a_9215719');

%% Plot
figure;

subplot(5, 1, 1);
title('Design Using Linearisation About Equilibrium Point A');
hold on;
stem(tda,x1da,'b');
plot(ta,x1a,'r');
hold off;
xlabel('Time (s)');
ylabel('Cart Pos. (m)');
legend('x_1 Discrete-Time Model', 'x_1 Sampled-Data Model');
grid on;

subplot(5, 1, 2);
hold on;
stem(tda,x2da*180/pi,'b');
plot(ta,x2a*180/pi,'r');
hold off;
xlabel('Time (s)');
ylabel('Pend. Ang. (�)');
legend('x_2 Discrete-Time Model', 'x_2 Sampled-Data Model');
grid on;

subplot(5, 1, 3);
hold on;
stem(tda,x3da,'b');
plot(ta,x3a,'r');
hold off;
xlabel('Time (s)');
ylabel('Cart Vel. (m/s)');
legend('x_3 Discrete-Time Model', 'x_3 Sampled-Data Model');
grid on;

subplot(5, 1, 4);
hold on;
stem(tda,x4da*180/pi,'b');
plot(ta,x4a*180/pi,'r');
hold off;
xlabel('Time (s)');
ylabel('Pend. Vel. (�/s)');
legend('x_4 Discrete-Time Model', 'x_4 Sampled-Data Model');
grid on;

subplot(5, 1, 5);
hold on;
stem(tda,Fda,'b');
plot(ta,Fa,'r');
hold off;
xlabel('Time (s)');
ylabel('Force (N)');
legend('F Discrete-Time Model', 'F Sampled-Data Model');
grid on;