function [G] = matexp(A, T, n)
%MATEXP Computer the matrix exponential e^AT using an approximation of order n.
    G = 0;
    for k = 0:n
       G = G + 1/factorial(k) * A^k * T^k;
    end
end

