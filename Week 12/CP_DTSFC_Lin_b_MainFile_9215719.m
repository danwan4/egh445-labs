clear;
close all;

%% Define model parameters
m = 0.15; % Pendulum mass (kg)
Mc = 0.4; % Cart mass (kg)
l = 0.2; % Pendulum arm length (m)
g = 9.81; % Gravity (m/s^2)

% Equilibrium point B
x1_bar = 0;
x2_bar = pi;
x3_bar = 0;
x4_bar = 0;

% Initial conditions
x0 = [0.2 - x1_bar; 200*pi/180 - x2_bar; 0 - x3_bar; 0 - x4_bar];

%% Compute matrices for linearised model about equilibrium point A
Ab = [0, 0, 1, 0; 
      0, 0, 0, 1; 
      0, -m*g/Mc, 0, 0; 
      0, -g*(Mc+m)/(l*Mc), 0, 0];
Bb = [0; 
      0;
      1/Mc;
      1/(l*Mc)];
Cb = eye(4);
Db = zeros(4, 1);

%% Create continuous-time state-space model
sysc = ss(Ab, Bb, Cb, Db);

%% Create discrete-time model
Ts = 0.03;
sysdzoh1 = c2d(sysc, Ts, 'zoh');
Adb = sysdzoh1.A;
Bdb = sysdzoh1.B;
Cdb = sysdzoh1.C;
Ddb = sysdzoh1.D;

%% Caclulate poles -3, -4, -5, -6 in Z-domain
Pdb = [exp(-3*Ts), exp(-4*Ts), exp(-5*Ts), exp(-6*Ts)];
Kdb = place(Adb, Bdb, Pdb);

%% Simulate discrete time model
sim('CP_DTSFC_Lin_b_9215719');

% Offset results for equilibrium point
% x1db = x1db + x1_bar;
% x2db = x2db + x2_bar;
% x3db = x3db + x3_bar;
% x4db = x4db + x4_bar;

%% Simulate sampled-data continuous-time model
Kb = Kdb;
sim('CP_SD_SFC_Lin_b_9215719');

% Offset results for equilibrium point
% x1b = x1b + x1_bar;
% x2b = x2b + x2_bar;
% x3b = x3b + x3_bar;
% x4b = x4b + x4_bar;

%% Plot
figure;

subplot(5, 1, 1);
title('Design Using Linearisation About Equilibrium Point B');
hold on;
stem(tdb,x1db,'b');
plot(tb,x1b,'r');
hold off;
xlabel('Time (s)');
ylabel('Cart Pos. (m)');
legend('x_1 Discrete-Time Model', 'x_1 Sampled-Data Model');
grid on;

subplot(5, 1, 2);
hold on;
stem(tdb,x2db*180/pi,'b');
plot(tb,x2b*180/pi,'r');
hold off;
xlabel('Time (s)');
ylabel('Pend. Ang. (�)');
legend('x_2 Discrete-Time Model', 'x_2 Sampled-Data Model');
grid on;

subplot(5, 1, 3);
hold on;
stem(tdb,x3db,'b');
plot(tb,x3b,'r');
hold off;
xlabel('Time (s)');
ylabel('Cart Vel. (m/s)');
legend('x_3 Discrete-Time Model', 'x_3 Sampled-Data Model');
grid on;

subplot(5, 1, 4);
hold on;
stem(tdb,x4db*180/pi,'b');
plot(tb,x4b*180/pi,'r');
hold off;
xlabel('Time (s)');
ylabel('Pend. Vel. (�/s)');
legend('x_4 Discrete-Time Model', 'x_4 Sampled-Data Model');
grid on;

subplot(5, 1, 5);
hold on;
stem(tdb,Fdb,'b');
plot(tb,Fb,'r');
hold off;
xlabel('Time (s)');
ylabel('Force (N)');
legend('F Discrete-Time Model', 'F Sampled-Data Model');
grid on;