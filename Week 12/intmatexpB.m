function [H] = intmatexpB(A,B,T,n)
%INTMAXEXPB Computes H using an approximation of order n.
    H = 0;
    for k = 0:n
       H = H + 1/factorial(k+1) * A^k * T^(k+1) * B;
    end
end

