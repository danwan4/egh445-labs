clear;
clc;

m = 0.15;
Mc = 0.4;
l = 0.2;
g = 9.81;

sim('Cart_Pendulum_Linearised_a_9215719.slx');

figure(1)
subplot(2,2,1)
plot(touta_tilde,x1a_tilde,'r',touta_tilde,Fa_tilde,'b')
title('x1','FontSize',18)
legend('Position [m]','Force [N]')
grid on
subplot(2,2,2)
plot(touta_tilde,x2a_tilde*(180/pi),'r',touta_tilde,Fa_tilde,'b')
title('x2','FontSize',18)
legend('Angle [degree]','Force [N]')
grid on
subplot(2,2,3)
plot(touta_tilde,x3a_tilde,'r',touta_tilde,Fa_tilde,'b')
title('x3','FontSize',18)
legend('Velocity [m/s]','Force [N]')
grid on
subplot(2,2,4)
plot(touta_tilde,x4a_tilde*(180/pi),'r',touta_tilde,Fa_tilde,'b')
title('x4','FontSize',18)
legend('Angular Velocity [degree/s]','Force [N]')
grid on