clear;
clc;

m = 0.15;
Mc = 0.4;
l = 0.2;
g = 9.81;

sim('Cart_Pendulum_9215719.slx');

figure(1)
subplot(2,2,1)
plot(tout,x1,'r',tout,F,'b')
title('x1','FontSize',18)
legend('Position [m]','Force [N]')
grid on
subplot(2,2,2)
plot(tout,x2*(180/pi),'r',tout,F,'b')
title('x2','FontSize',18)
legend('Angle [degree]','Force [N]')
grid on
subplot(2,2,3)
plot(tout,x3,'r',tout,F,'b')
title('x3','FontSize',18)
legend('Velocity [m/s]','Force [N]')
grid on
subplot(2,2,4)
plot(tout,x4*(180/pi),'r',tout,F,'b')
title('x4','FontSize',18)
legend('Angular Velocity [degree/s]','Force [N]')
grid on