clear;
close all;

%% Define model parameters
m = 0.15; % Pendulum mass (kg)
Mc = 0.4; % Cart mass (kg)
l = 0.2; % Pendulum arm length (m)
g = 9.81; % Gravity (m/s^2)
x0 = [0.2; 20*pi/180; 0; 0]; % Initial conditions

%% Compute matrices for linearised model about equilibrium point A
Aa = [0, 0, 1, 0; 
      0, 0, 0, 1; 
      0, -m*g/Mc, 0, 0; 
      0, g*(Mc+m)/(l*Mc), 0, 0];
Ba = [0;
      0;
      1/Mc;
      -1/(l*Mc)];
Ca = eye(4);
Da = zeros(4, 1);

%% Verify controllability
controllable = rank(ctrb(Aa, Ba)) == 4;

%% Compute controller gain for desired eigenvalues -3, -4, -5, -6
Pa = [-3, -4, -5, -6];
Ka = place(Aa, Ba, Pa);

%% Simulate linearised model
sim('CP_SFC_Lin_a_9215719');

%% Simulate nonlienar model
K_SF = Ka; % Select control gain K_SF as Ka
sim('CP_SFC_NLin_9215719');

%% Plot
figure;

subplot(5, 1, 1);
plot(t,x1,'b',ta,x1a,'r--');
title('Design Using Linearisation About Equilibrium Point A');
% title('x_1');
xlabel('Time (s)');
ylabel('Cart Pos. (m)');
legend('x_1 Nonlinear', 'x_1 Linearised');
grid on;

subplot(5, 1, 2);
plot(t,x2*180/pi,'b',ta,x2a*180/pi,'r--');
% title('x_2');
xlabel('Time (s)');
ylabel('Pend. Ang. (�)');
legend('x_2 Nonlinear', 'x_2 Linearised');
grid on;

subplot(5, 1, 3);
plot(t,x3,'b',ta,x3a,'r--');
% title('x_3');
xlabel('Time (s)');
ylabel('Cart Vel. (m/s)');
legend('x_3 Nonlinear', 'x_3 Linearised');
grid on;

subplot(5, 1, 4);
plot(t,x4*180/pi,'b',ta,x4a*180/pi,'r--');
% title('x_4');
xlabel('Time (s)');
ylabel('Pend. Vel. (�/s)');
legend('x_4 Nonlinear', 'x_4 Linearised');
grid on;

subplot(5, 1, 5);
plot(t,F,'b',ta,Fa,'r--');
% title('Force');
xlabel('Time (s)');
ylabel('Force (N)');
legend('F Nonlinear', 'F Linearised');
grid on;

%% Animation
% Cart_Pendulum_Animation(ta, x1a, x2a, 0, 0);
% Cart_Pendulum_Animation(t, x1, x2, 0, 0);