clear;
close all;

%% Define model parameters
m = 0.15; % Pendulum mass (kg)
Mc = 0.4; % Cart mass (kg)
l = 0.2; % Pendulum arm length (m)
g = 9.81; % Gravity (m/s^2)

%% Compute matrices for linearised model about equilibrium point A
Ab = [0, 0, 1, 0; 
      0, 0, 0, 1; 
      0, -m*g/Mc, 0, 0; 
      0, -g*(Mc+m)/(l*Mc), 0, 0];
Bb = [0;
      0;
      1/Mc;
      1/(l*Mc)];
Cb = eye(4);
Db = zeros(4, 1);

%% Create continuous-time state-space model
sysc = ss(Ab, Bb, Cb, Db);

Ts = [0.1, 0.01];

for i = 1:length(Ts)
    %% Create discrete-time model
    sysdzoh2 = c2d(sysc, Ts(i), 'zoh');

    %% Compute the matrix G using first, second, third and fourth order
    %% approximation of the matrix exponential
    I = eye(4);
    G21 = I + Ab*Ts(i);
    G22 = G21 + 1/2*Ab*Ab*Ts(i)^2;
    G23 = G22 + 1/6*Ab*Ab*Ab*Ts(i)^3;
    G24 = G23 + 1/24*Ab*Ab*Ab*Ab*Ts(i)^4;

    H2 = sysdzoh2.B;

    %% Is H = A^-1*(G-I)*B computable?
    A_invertible = det(Ab) ~= 0; % No

    %% Create four discrete-time models using the approximations
    sysd21 = ss(G21, H2, Cb, Db, Ts(i));
    sysd22 = ss(G22, H2, Cb, Db, Ts(i));
    sysd23 = ss(G23, H2, Cb, Db, Ts(i));
    sysd24 = ss(G24, H2, Cb, Db, Ts(i));

    figure;
    step(sysc, sysdzoh2, sysd21, sysd22, sysd23, sysd24, 2);
    legend('sysc', 'sysdzoh2', 'sysd21', 'sysd22', 'sysd23', 'sys24');
    title("Equilibrium Point B: T_s = " + Ts(i) + " sec");
end