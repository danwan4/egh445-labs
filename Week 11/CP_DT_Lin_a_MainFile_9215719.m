clear;
close all;

%% Define model parameters
m = 0.15; % Pendulum mass (kg)
Mc = 0.4; % Cart mass (kg)
l = 0.2; % Pendulum arm length (m)
g = 9.81; % Gravity (m/s^2)

%% Compute matrices for linearised model about equilibrium point A
Aa = [0, 0, 1, 0; 
      0, 0, 0, 1; 
      0, -m*g/Mc, 0, 0; 
      0, g*(Mc+m)/(l*Mc), 0, 0];
Ba = [0;
      0;
      1/Mc;
      -1/(l*Mc)];
Ca = eye(4);
Da = zeros(4, 1);

%% Create continuous-time state-space model
sysc = ss(Aa, Ba, Ca, Da);

Ts = [0.1, 0.01];

for i = 1:length(Ts)
    %% Create discrete-time model
    sysdzoh1 = c2d(sysc, Ts(i), 'zoh');

    %% Compute the matrix G using first, second, third and fourth order
    %% approximation of the matrix exponential
    I = eye(4);
    G11 = I + Aa*Ts(i);
    G12 = G11 + 1/2*Aa*Aa*Ts(i)^2;
    G13 = G12 + 1/6*Aa*Aa*Aa*Ts(i)^3;
    G14 = G13 + 1/24*Aa*Aa*Aa*Aa*Ts(i)^4;

    H1 = sysdzoh1.B;

    %% Is H = A^-1*(G-I)*B computable?
    A_invertible = det(Aa) ~= 0; % No

    %% Create four discrete-time models using the approximations
    sysd11 = ss(G11, H1, Ca, Da, Ts(i));
    sysd12 = ss(G12, H1, Ca, Da, Ts(i));
    sysd13 = ss(G13, H1, Ca, Da, Ts(i));
    sysd14 = ss(G14, H1, Ca, Da, Ts(i));

    figure;
    step(sysc, sysdzoh1, sysd11, sysd12, sysd13, sysd14, 0.8);
    legend('sysc', 'sysdzoh1', 'sysd11', 'sysd12', 'sysd13', 'sys14');
    title("Equilibrium Point A: T_s = " + Ts(i) + " sec");
end